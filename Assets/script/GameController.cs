﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	//singleton pattern ==there cna only be one!
	private static GameController instance;

	public static GameController Instance {
		get {
			if (instance == null) {
				instance = GameObject.FindObjectOfType<GameController>();
				//prevents this object from being destroyed
				DontDestroyOnLoad (instance.gameObject);
			}
			return instance;
		}
	}

	void Awake() {
		if (instance == null) {
			instance= this;
			DontDestroyOnLoad (this.gameObject);
		}else { 
			if (this!= instance) {
				//destroy the imitator
				Destroy (this.gameObject);
			}

		}
		init_UI ();
	}
	//end of the pattern

	// THhe GAME CONTROLLER specific code

	private const string SCORE_TEXT= "SCORE:";
	private UILabel label;

	private float points;

	void init_UI ()
	{
		label = gameObject.GetComponent<UILabel> ();
	}

	void Update()
	{
		if (label!=null) {
			label.Text = SCORE_TEXT+ points.ToString();

		}
	}

	public void AddPoints (float amount)
	{
		points += amount;
	}

}
