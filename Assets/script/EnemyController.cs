﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public bool canMove= false;

	public GameObject player;
	public float attackRange = 10.0f; 
	public Transform firePoint;

	public GameObject projectile;//the bullet

	public float fireRate=1.0f;// how often it fires
	private float nextFire;//
	public bool doesNotDisengage= false;
	private bool playerFound=false;

	private float damping = 6.0f;
	public float movementSpeed=0.1f;

	private Vector3 targetDirection;
	private float targetAngle;
	private Rigidbody2D thisRigidbody;
	// Use this for initialization
	void Start () {
		playerFound = false;

		thisRigidbody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (player)
		{
			if (attackRange>Vector3.Distance(player.transform.position,
			                                transform.position)|| playerFound)
			{
				if (doesNotDisengage) {playerFound = true;}

				targetDirection = player.transform.position - transform.position;
				targetAngle = Mathf.Atan2 (targetDirection.y,targetDirection.x) * Mathf.Rad2Deg;
				transform.rotation= Quaternion.AngleAxis (targetAngle, Vector3.forward);

				//rigidbody2D.AddRelativeForce (new Vector2(0,movementSpeed));
				move ();
				if (Time.time>nextFire)
				{
					//^//
					nextFire=Time.time + fireRate;
					GameObject bullet = Instantiate(projectile,
					                                firePoint.position,
					                                firePoint.rotation) as GameObject;
					bullet.GetComponent<Projectile>().CreatedBy("Enemy");
				}

			}else{
				patrol();
			}
		}else {
			player=GameObject.FindGameObjectWithTag("Player");
		}
	}
	public Transform[] wayPoints;
	public int currentWP;
	public float distanceToWayPoint=2.0f; // we're close enough

	private void patrol ()
	{
		if (wayPoints.Length == 0) {
			return;		
		}
		if (wayPoints[currentWP] == null) {
			nextWayPoint();
			return;
		}
		if (distanceToWayPoint > Vector3.Distance (wayPoints [currentWP].position,
		                                           transform.position)) {
						nextWayPoint ();	
				} else {
						transform.LookAt (wayPoints [currentWP]);
						//add force or speed 
				}
	}

	private void nextWayPoint()
	{

		currentWP++;
		if (currentWP > wayPoints.Length - 1) {
			currentWP = 0;		
		}
	}

	public float maxSpeed=1.0f;
	public float moveForce=150.0f;
	private void move()
	{	if (! canMove)return;

		if  (Mathf.Abs(thisRigidbody.velocity.x) < maxSpeed && 
			Mathf.Abs(thisRigidbody.velocity.x) <maxSpeed)
		{
			thisRigidbody.AddForce(transform.right*moveForce);		
		}
	}
}
