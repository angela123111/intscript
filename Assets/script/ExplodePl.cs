﻿using UnityEngine;
using System.Collections;

public class ExplodePl : MonoBehaviour {
	public GameObject explosion;


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) {
			Debug.Log ("BOOM");	

			Instantiate (explosion, 
			             other.transform.position,
			             other.transform.rotation);
			
			Destroy (other.gameObject);
		}
	}
}
