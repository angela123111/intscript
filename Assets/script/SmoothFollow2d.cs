﻿using UnityEngine;
using System.Collections;

public class SmoothFollow2d : MonoBehaviour {

	public Transform target; 
	public float smoothTime = 0.3f;

	private Transform thisTransform;
	private Vector2 velocity; 


	// Use this for initialization
	void Start () {
		thisTransform = this.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (target) {
			float x = Mathf.SmoothDamp (thisTransform.position.x, 
			                            target.position.x,
			                            ref velocity.x,
			                            smoothTime);
			float y = Mathf.SmoothDamp (thisTransform.position.y, 
			                            target.position.y,
			                            ref velocity.y,
			                            smoothTime);
			transform.position = new Vector3 (x,y,thisTransform.position.z);
		}
	}
}
