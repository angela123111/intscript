﻿using UnityEngine;
using System.Collections;

public class WeaponControl : MonoBehaviour {
	private float nextFire=0.0f;
	public float fireRate=1.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	void Update (){
				if (Time.time > nextFire) {
						if (Input.GetMouseButton (0) || Input.GetKey (KeyCode.Space)) {
								nextFire = Time.time + fireRate;
								fireWeapon ();
								//fire the weapon.
						}
				}
		}
	public Transform firePoint;
	public GameObject projectilePrefab;
	private void fireWeapon ()
	{
		if (firePoint)
		{
			GameObject proj = Instantiate(
				projectilePrefab, 
				firePoint.position, firePoint.rotation) as GameObject;
			proj.GetComponent<Projectile>().CreatedBy("Player");
			
		}
	}
}
